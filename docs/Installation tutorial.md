
@@ -3,184 +3,186 @@

> [MeEdu common problems summary and function introduction] (https://www.yuque.com/meedu/yr7rek)

#### Environmental requirements

+ Composer
+ PHP >= 7.2
+ MySql >= 5.6
+ Zip PHP Extension
+ OpenSSL PHP Extension
+ PDOMysql PHP Extension
+ Mbstring PHP Extension
+ Tokenizer PHP Extension
+ XML PHP Extension
+ Fileinfo PHP Extension

## Step

#### step one

Install meedu

```
composer create-project qsnh/meedu
git clone https://github.com/Qsnh/meedu.git meedu
composer install --no-dev
composer dump-autoload
```

#### Step 2

Generate JWT key:

```
php artisan key:generate
php artisan jwt:secret
```

Configure the database, open the .env file, and modify the following:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

Configure basic information

```
APP_NAME=MeEdu
APP_ENV=local (if it is officially running here, please modify to: production)
APP_KEY=
APP_DEBUG=true (If it is officially running here, please modify to: false)
APP_LOG_LEVEL=debug
APP_URL=http://localhost (modify your own address here)
```

#### Step Three

Create a soft link to the upload directory:

```
php artisan storage:link
```

#### Step 4

Set `storage` directory permissions ` 777`

```
chmod -R 0777 storage
```

#### Step 5

Configure pseudo-static and set the running directory of meedu to `public` .

Pseudo-Static Rule (Nginx):

```
location / {  
	try_files $uri $uri/ /index.php$is_args$query_string;  
}
```

#### Step 6

Installation data sheet

```
php artisan migrate
```

#### Step 7

Initialize system permissions:

```
php artisan install role
```

Initialize the administrator:

```
php artisan install administrator
```

> Installation prompt to enter the administrator account and password!

  
**Generate installation lock🔐 file**

```
php artisan install:lock
```

#### Step 8

At this point, MeEdu is basically installed. However, if you want to access the MeEdu backend, you also need to install the MeEdu backend front-end project, address:
[ https://github.com/Meedu/backend ]( https://github.com/Meedu/backend ) . Please follow the instructions of the project to install.

## If you officially use MeEdu in the production environment, the following needs to be configured

#### Step 9

Configure the task scheduler:

Add the following to the crontab scheduled task:

```
* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
```

> Note to replace the path where the meedu is located


### Step 10

Configure the queue listener:

First, install supervisor:

```
sudo apt install supervisor
```

Configure supervisor 

```
cd /etc/supervisor/conf.d
vi meedu.conf
```

Paste the following content:

```
[program:meedu]
process_name=%(program_name)s_%(process_num)02d
command=php /you-project-path/artisan queue:work --sleep=3 --tries=3
autostart=true
autorestart=true
user=root
numprocs=1
redirect_stderr=true
stdout_logfile=/you-project-path/storage/logs/supervisor.log
```

> Pay attention to replace the path and user

Restart the service:

``` angular2html
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start meedu:*
```

If the configuration is correct, you will see the output log of supervisor, the log path is the value of stdout_logfile configured above.

## common problem

#### Mysql8 database

The default configuration of mysql8 is changed to default-authentication-plugin=mysql_native_password